EESchema Schematic File Version 2
LIBS:hpr-telem-board-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:w_analog
LIBS:sprk_rfm69hcw
LIBS:gy-gps6mv2
LIBS:ESP8266
LIBS:bme280
LIBS:si1401edh
LIBS:hpr-telem-board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BME280 U5
U 1 1 58BF8328
P 5850 3950
F 0 "U5" H 6050 4250 60  0000 C CNN
F 1 "BME280" V 5850 3950 60  0000 C CNN
F 2 "libs:BME280" H 5850 3950 60  0001 C CNN
F 3 "" H 5850 3950 60  0001 C CNN
	1    5850 3950
	1    0    0    -1  
$EndComp
Text HLabel 7700 3550 2    60   Input ~ 0
VDD
$Comp
L GND #PWR017
U 1 1 58BF83FC
P 5350 3250
F 0 "#PWR017" H 5350 3000 50  0001 C CNN
F 1 "GND" H 5350 3100 50  0000 C CNN
F 2 "" H 5350 3250 50  0000 C CNN
F 3 "" H 5350 3250 50  0000 C CNN
	1    5350 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6350 3800 7200 3800
Connection ~ 6600 3800
Wire Wire Line
	5350 3250 6600 3250
Wire Wire Line
	5350 3250 5350 3800
$Comp
L C C13
U 1 1 58BF846D
P 6600 3400
F 0 "C13" H 6625 3500 50  0000 L CNN
F 1 "100n" H 6625 3300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6638 3250 50  0001 C CNN
F 3 "" H 6600 3400 50  0000 C CNN
	1    6600 3400
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 3550 6600 3800
$Comp
L GND #PWR018
U 1 1 58BF84B3
P 6550 4550
F 0 "#PWR018" H 6550 4300 50  0001 C CNN
F 1 "GND" H 6550 4400 50  0000 C CNN
F 2 "" H 6550 4550 50  0000 C CNN
F 3 "" H 6550 4550 50  0000 C CNN
	1    6550 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3900 6550 3900
Wire Wire Line
	6550 3900 6550 4550
Wire Wire Line
	6350 4100 6550 4100
Connection ~ 6550 4100
Text HLabel 7700 3650 2    60   Input ~ 0
VDDIO
Wire Wire Line
	6350 4000 6800 4000
$Comp
L C C14
U 1 1 58BF84FA
P 6700 4300
F 0 "C14" H 6725 4400 50  0000 L CNN
F 1 "100n" H 6725 4200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 6738 4150 50  0001 C CNN
F 3 "" H 6700 4300 50  0000 C CNN
	1    6700 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4150 6700 4000
Connection ~ 6700 4000
Wire Wire Line
	6700 4450 6550 4450
Connection ~ 6550 4450
Wire Wire Line
	5350 3900 5000 3900
Wire Wire Line
	5000 3900 5000 4450
Wire Wire Line
	6400 4450 6400 4000
Connection ~ 6400 4000
$Comp
L R R9
U 1 1 58BF858B
P 5350 4300
F 0 "R9" V 5430 4300 50  0000 C CNN
F 1 "4.7k" V 5350 4300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 5280 4300 50  0001 C CNN
F 3 "" H 5350 4300 50  0000 C CNN
	1    5350 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4450 6400 4450
Connection ~ 5350 4450
Wire Wire Line
	5350 4150 5350 4100
Wire Wire Line
	5350 4100 4600 4100
Text HLabel 4600 4100 0    60   Input ~ 0
BME_SCK
$Comp
L R R8
U 1 1 58BF8674
P 5150 4300
F 0 "R8" V 5230 4300 50  0000 C CNN
F 1 "4.7k" V 5150 4300 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 5080 4300 50  0001 C CNN
F 3 "" H 5150 4300 50  0000 C CNN
	1    5150 4300
	1    0    0    -1  
$EndComp
Connection ~ 5150 4450
Wire Wire Line
	5150 4150 5150 4000
Wire Wire Line
	4600 4000 5350 4000
Connection ~ 5150 4000
Text HLabel 4600 4000 0    60   BiDi ~ 0
BME_SDA
Connection ~ 5350 3250
$Comp
L Si1401EDH Q4
U 1 1 58C7FACF
P 7400 3900
F 0 "Q4" H 7700 3950 50  0000 R CNN
F 1 "Si1401EDH" H 8025 3850 50  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SC-70-6_Handsoldering" H 7600 4000 50  0001 C CNN
F 3 "" H 7400 3900 50  0000 C CNN
	1    7400 3900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7700 3800 7600 3800
Wire Wire Line
	7700 3550 7700 3800
Wire Wire Line
	6800 4000 6800 3800
Connection ~ 6800 3800
Wire Wire Line
	7100 3700 7200 3700
Wire Wire Line
	7100 3500 7100 3800
Connection ~ 7100 3800
Wire Wire Line
	7200 3600 7100 3600
Connection ~ 7100 3700
Wire Wire Line
	7200 3500 7100 3500
Connection ~ 7100 3600
$Comp
L R R13
U 1 1 58C812DF
P 7700 3950
F 0 "R13" V 7780 3950 50  0000 C CNN
F 1 "100k" V 7700 3950 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7630 3950 50  0001 C CNN
F 3 "" H 7700 3950 50  0000 C CNN
	1    7700 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 4100 8050 4100
Connection ~ 7700 4100
Text HLabel 8050 4100 2    60   Input ~ 0
BME_EN
$Comp
L PWR_FLAG #FLG019
U 1 1 58C8D246
P 6450 3800
F 0 "#FLG019" H 6450 3895 50  0001 C CNN
F 1 "PWR_FLAG" H 6450 3980 50  0000 C CNN
F 2 "" H 6450 3800 50  0000 C CNN
F 3 "" H 6450 3800 50  0000 C CNN
	1    6450 3800
	1    0    0    -1  
$EndComp
Connection ~ 6450 3800
$Comp
L CONN_01X02 P7
U 1 1 58CAE311
P 4800 4600
F 0 "P7" H 4800 4750 50  0000 C CNN
F 1 "CONN_01X02" V 4900 4600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 4800 4600 50  0001 C CNN
F 3 "" H 4800 4600 50  0000 C CNN
	1    4800 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 4400 4750 4100
Connection ~ 4750 4100
Wire Wire Line
	4850 4400 4850 4000
Connection ~ 4850 4000
$EndSCHEMATC
